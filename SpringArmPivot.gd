extends Node3D

var x_mouse_sens = 0.1
var y_mouse_sens = 0.1 

# Called when the node enters the scene tree for the first time.
@onready
var spring_arm3d = $SpringArm3D
@onready
var character = get_node("..")

func _unhandled_input(event):
	if event is InputEventMouseMotion:
		spring_arm3d.rotate_x(deg_to_rad(-event.relative.y * y_mouse_sens))
		rotate_y(deg_to_rad(-event.relative.x * x_mouse_sens))


func _ready():
	#set_as_top_level(true)
	Input.mouse_mode = Input.MOUSE_MODE_CAPTURED


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	spring_arm3d.rotation_degrees.x = clamp(spring_arm3d.rotation_degrees.x, -80.0, 30.0)
	rotation_degrees.y = wrapf(rotation_degrees.y, 0.0, 360.0)

	#look_at(character.velocity)
