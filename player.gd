extends CharacterBody3D


#const SPEED = 5.0
const JUMP_VELOCITY = 5
const FRICTION = 0.7
const AIR_ACCEL = 5
const GROUND_ACCEL = 25
const MAX_GROUND_VEL = 7
const MAX_AIR_VEL = 5
const LERP_SLOWDOWN = 8.0
const MOVETOWARD_SLOWDOWN = 0.6
const FORWARD_JUMP_STRENGTH = 3

# Get the gravity from the project settings to be synced with RigidBody nodes.
var gravity = ProjectSettings.get_setting("physics/3d/default_gravity")
#var facing_direction = 

@onready
var spring_arm_pivot = $SpringArmPivot
@onready
var collision_shape_3d = $CollisionShape3D
@onready
var model = $Model

func SLOWDOWN(prevVelocity:Vector3, delta) -> Vector3:
	velocity.x = lerp(velocity.x, 0.0, LERP_SLOWDOWN*delta)
	velocity.z = lerp(velocity.z, 0.0, LERP_SLOWDOWN*delta)
	velocity.x = move_toward(velocity.x, 0, MOVETOWARD_SLOWDOWN*delta)
	velocity.z = move_toward(velocity.z, 0, MOVETOWARD_SLOWDOWN*delta)
	return velocity

func ACCELERATE(accelDir:Vector3, prevVelocity:Vector3, accelerate:float, max_velocity:float, delta) -> Vector3:
	var projVel = prevVelocity.dot(accelDir) # Vector projection of Current velocity onto accelDir.
	var accelVel = accelerate * delta # Accelerated velocity in direction of movment

	# If necessary, truncate the accelerated velocity so the vector projection does not exceed max_velocity
	if(projVel + accelVel > max_velocity):
		accelVel = max_velocity - projVel

	return prevVelocity + accelDir * accelVel

func MOVE_GROUND(accelDir:Vector3, prevVelocity:Vector3, delta) -> Vector3:
	var vert_vel = prevVelocity.y
	var speed = prevVelocity.length()
	if prevVelocity != Vector3.ZERO:
		var drop = speed * FRICTION * delta
		prevVelocity *= maxf(speed - drop, 0) / speed
	prevVelocity.y = vert_vel
	return ACCELERATE(accelDir, prevVelocity,GROUND_ACCEL, MAX_GROUND_VEL, delta)

func MOVE_AIR(accelDir:Vector3, prevVelocity:Vector3, delta) -> Vector3:	
	return ACCELERATE(accelDir, prevVelocity,GROUND_ACCEL, MAX_AIR_VEL, delta)	

	
func _physics_process(delta):
	# Add the gravity.
	var isOnFloor = is_on_floor()
	if not isOnFloor:
		velocity.y -= gravity * delta
		
	# Get the input direction and handle the movement/deceleration.
	# As good practice, you should replace UI actions with custom gameplay actions.
	var input_dir = Input.get_vector("left", "right", "up", "down")
	var direction = (transform.basis * Vector3(input_dir.x, 0, input_dir.y))
	direction = direction.rotated(Vector3.UP, spring_arm_pivot.rotation.y)
	if direction:
		if direction.length()>1.0:
			direction = direction.normalized()
		if isOnFloor and is_zero_approx(velocity.y):
			if Input.is_action_just_pressed("jump"):
				velocity.y = JUMP_VELOCITY
				velocity.move_toward(direction, 3*FORWARD_JUMP_STRENGTH) #do I like this? do I understand this? idt so
				velocity.x +=direction.x*FORWARD_JUMP_STRENGTH
				velocity.z +=direction.z*FORWARD_JUMP_STRENGTH
			velocity = MOVE_GROUND(direction, velocity, delta)
		else:
			velocity = MOVE_AIR(direction,velocity, delta)
	else:
		if isOnFloor:
			velocity = SLOWDOWN(velocity, delta)
			if Input.is_action_just_pressed("jump"):
				velocity.y = JUMP_VELOCITY
	#print(velocity.length())
	if velocity.length() > 0.2:
		model.rotation.y = Vector2(-velocity.z, -velocity.x).angle()
	move_and_slide()
#	collision_shape_3d.rotation.z = new Vector2(veclocity.x, velocity.z).angle() 

